from .base import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

USER_AUTH_TOKEN_MAX_AGE = 7
INSTALLED_APPS.append('debug_toolbar')

MIDDLEWARE.insert(
    MIDDLEWARE.index('django.middleware.common.CommonMiddleware') + 1,
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)
