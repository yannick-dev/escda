try:
    print('Trying import local.py settings...')
    from .local import *  # noqa
except ImportError:
    print('Trying import development.py settings...')
    from .dev import *  # noqa
