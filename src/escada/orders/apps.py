from django.apps import AppConfig


class OrdersConfig(AppConfig):
    name = 'escada.orders'
