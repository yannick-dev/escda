from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.
BRANDS = (
           ('200708','ESCADA ML'),
           ('201391','ESCADA SPORT')
         )

SEASONS = (
               ('1_CS','SUMMER'),
               ('1_MM','SPRING'),
               ('2_CS','FALL'),
               ('2_MM','WINTER')
              )

STATUS = (
    ('CR','CREATED'),
    ('AW','AWAITING'),
    ('DL','DELIVERED'),
    ('BT','BEEN TREAT'),
    ('CA','CANCEL'),
    ('UN','UNDEFINED')
)

COLLECTIONS = (
    ('CR','ОСНОВНАЯ'),
    ('PR','ПРЕДВАРИТЕЛЬНАЯ')
)

SIZE = ('one',('one','ONE'),
        ('two', (('xl','XL'),('s','S'),('m','M'),('l','L'),('xs','XS'))),
        ('three',(('30','30'),('36','38'))),
        ('four',('45','45')),
        ('five',('60','30'))
)

class Order(models.Model):
    _name = models.CharField(blank=False, max_length=300, unique=True)
    _brand = models.CharField(_('Бренд'), default='ESCADA ML', max_length=25, choices=BRANDS)
    _collection = models.CharField(_('Коллекция'), max_length=25, choices=COLLECTIONS)
    _season = models.CharField(_('Сезон'), max_length=25, choices=SEASONS)
    _created= models.DateTimeField(auto_now_add=True)
    _status= models.CharField(blank=False, max_length=200)


class Item(models.Model):
    _name = models.CharField(blank=False,max_length=300)
    _number= models.PositiveIntegerField(blank=False)
    _group = models.CharField(_('Группа'), max_length=200, )
    _size = models.CharField(_('Размер'), max_length=4)
    _sub_group = models.CharField(_('Наименование'), max_length=200)
    _vendor_code = models.CharField(_('Артикул'), max_length=25)
    _color =       models.CharField(_('Цвет'), max_length=50)
    _code_color = models.CharField(_('Code Цвет'), max_length=50)
    _description = models.TextField(_('Описание'), max_length=1000)
    _composition = models.TextField(_('Состав'), max_length=1000,blank=True)
    _quantity = models.PositiveIntegerField(_('Количество'), )
    _price = models.FloatField(blank=False)
    _picture = models.ImageField(upload_to='pictures/', null=True, blank=True)
    _total_cost = models.FloatField(editable=False)
    _order = models.ForeignKey(Order,related_name='_items' ,default=1,on_delete=models.CASCADE)


    class Meta:
        ordering=['_number']


    def save(self,*args,**kwargs):
        self._total_cost = abs(self._price) * self._quantity
        super(Item, self).save(*args, **kwargs)







class ConfirmationFile(models.Model):
    name = models.CharField(max_length=500 , blank=False,unique=True)
    confirmation = models.FileField(blank=True,null=True)
    invoice = models.FileField(blank=True,null=True)
