from django.conf.urls import url
from rest_framework.routers import SimpleRouter
from .viewsets import OrderViewSet,FileUploadViewSet

router =SimpleRouter(trailing_slash=False)
router.register(r'order',OrderViewSet,base_name='order')
router.register(r'upload',FileUploadViewSet,base_name='upload')


urlpatterns = router.get_urls()