from django.core.exceptions import ObjectDoesNotExist
from rest_framework import viewsets,status,permissions
from rest_framework.decorators import list_route, detail_route
from rest_framework.parsers import FileUploadParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from .serializers import ItemSerializer,OrderSerializer,ConfirmationFileSerializer
from .models import Item, ConfirmationFile,Order
from escada.utils import xlsxparser
import json

class OrderViewSet(viewsets.ModelViewSet):
    """
    create ,retrieve update and delete orders
    """
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    permission_classes = [permissions.AllowAny]
    #
    # @list_route(methods=['post'])
    # def create_supply(self,request):
    #
    #     serializer = OrderSerializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response({'created':True},status=status.HTTP_201_CREATED)
    #
    #
    # @detail_route(methods=['post'])
    # def parse_command(self,request):
    #     pass



class FileUploadViewSet(viewsets.ModelViewSet):
    """
    get post and update invoice and confirmation file 
    """
    serializer_class = ConfirmationFileSerializer
    queryset = ConfirmationFile.objects.all()
    permission_classes = [permissions.AllowAny]

    #parser_classes = FileUploadParser
    #
    # @detail_route(methods=['post'])
    # def parse_order(self,request,pk=None):
    #     #order = Order.objects.get(pk=request.data['order'])
    #     confirmation= ConfirmationFile.objects.get(pk=pk)
    #     if confirmation :
    #         invoice = confirmation.invoice
    #         confirmation_list = confirmation.confirmation
    #         json_invoice = xlsxparser.excel_to_json(str(invoice))
    #         json_confirmation = xlsxparser.excel_to_json(str(confirmation_list))
    #         result = xlsxparser.parse_command(json_invoice,json_confirmation)
    #         if not result:
    #             return Response({'errors': 'something went wrong'}, status=status.HTTP_400_BAD_REQUEST)
    #         return Response({'result':result }, status=status.HTTP_200_OK)
    #     return Response({'state':'failed to identity the command'})

    @list_route(methods=['post'] ,permission_classes=[permissions.AllowAny])
    def parse (self,request):
        name = request.data['name']
        order = Order.objects.get(_name__exact=name)
        confirmation=None
        if name and order :
            try :
                confirmation = ConfirmationFile.objects.get(name__exact=name)
            except ObjectDoesNotExist:
                serializer = ConfirmationFileSerializer(data=request.data)
                if serializer.is_valid(raise_exception=True):
                    confirmation=serializer.save()
            confirmation_ = confirmation
            if confirmation_.confirmation :
                order_serializer = OrderSerializer(order)
                result = xlsxparser.parse_command(str(confirmation_.confirmation),order_serializer.data)
                return Response({'conflicts':result},status=status.HTTP_200_OK)
        return Response({'error':'something went wrong'},status=status.HTTP_400_BAD_REQUEST)


    def patch(self,request,pk=None):
        file = request.FILES.get('file')
        confirmation =self.get_object()
        if confirmation:
            serializer = ConfirmationFileSerializer(confirmation)
            serializer.save()



class CompareCommand:
    pass
