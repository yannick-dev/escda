from rest_framework import serializers
from .models import Item,Order,ConfirmationFile
from django.core.exceptions import ObjectDoesNotExist,ValidationError
from django.db.models import ObjectDoesNotExist






class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('_number','_group','_vendor_code','_order',''
                  '_description','_price','_size','_code_color',
                  '_quantity','_color','_composition')


    # def create(self, validated_data):
    #     color = validated_data.pop('color',None)
    #     product = validated_data.pop('product',None)
    #     if color and product :
    #         print(color['name'],color['code'],product['group'])
    #         try:
    #             col = Color.objects.get(name=color['name'],code=color['name'])
    #             validated_data.__setitem__('color',col)
    #         except ObjectDoesNotExist:
    #             col = Color(**color)
    #             col.save()
    #             validated_data.__setitem__('color', col)
    #
    #         try:
    #             prod = Group.objects.get(**product)
    #             validated_data.__setitem__('product',prod)
    #
    #         except ObjectDoesNotExist:
    #             prod =Group(**product)
    #             prod.save()
    #             validated_data.__setitem__('product',prod)
    #         try:
    #             order = Item.objects.create(**validated_data)
    #         except (Exception,ValueError,ValidationError):
    #             col.delete()
    #             prod.delete()
    #             return None
    #
    #         return order


    def update(self, instance, validated_data):
        raise NotImplementedError('Method not supported')



class OrderSerializer(serializers.ModelSerializer):
    _items = ItemSerializer(many=True)
    class Meta:
        model=Order
        fields=('_name','_status','_brand','_season','_collection','_items')

    def create(self, validated_data):
        items = validated_data.pop('_items')
        order = Order.objects.create(**validated_data)

        for item in items:
           Item.objects.create(**item)
        return order



class ConfirmationFileSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=500,allow_blank=True)
    class Meta:
        model= ConfirmationFile
        fields=('invoice','confirmation','name')

    def create(self, validated_data):
        return ConfirmationFile.objects.create(**validated_data)
    # def create(self, validated_data):
    #
    #     name = validated_data.pop('name')
    #     validated_data.pop('order')
    #     try:
    #         order = Order.objects.get(_name__exact=name)
    #     except ObjectDoesNotExist:
    #         raise ObjectDoesNotExist('not such object was found')
    #
    #     validated_data.__setitem__('order',order.pk)
    #     files = ConfirmationFile.objects.create(**validated_data)
    #     return files





