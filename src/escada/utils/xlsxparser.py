import xlrd
import json,math
from collections import OrderedDict
heading_en =['_brand', '_season', '_collection', '_theme', '_vendor_code', '_group'
          ,'supgroup','_class','_color','_size','_quantity','_price','_total','currency','del','composition']

def convert_oder(order):
    list_items = []
    name = order['_name']
    season = order['_season']
    brand = order['_brand']
    collection = order['_collection']
    items = order['_items']
    for index in range(0,items.__len__()):
        item =OrderedDict(**items[index])
        item['_name']=name
        item['_season']=season
        item['_brand']=brand
        item['_collection']=collection
        list_items.append(item)
    return list_items


def parse_command(file,order):
    path = "escada/media/" + file
    workbook = xlrd.open_workbook(path)
    worksheet = workbook.sheet_by_index(0)
    order = convert_oder(order)
    list_conflict = []
    iter_ =0
    for index in range(1, worksheet.nrows):
        row_iterator = worksheet.row_values(index)
        if not row_iterator[0] or row_iterator[0] == "":
            continue

        iter_+=iter_
        item = OrderedDict()

        for col in range(0, heading_en.__len__()):
            key = heading_en[col]
            item[key] = row_iterator[col]

        if order[iter_]:
            item_order = order[iter_]
            if item != item_order:
                list_conflict.append({'commands': item_order, 'invoice': item})
        else:

            list_conflict.append({'commands':'none','invoice':item})

    result = OrderedDict()
    result['number']=list_conflict.__len__()
    result['detail']=list_conflict

    return json.dumps(result)





